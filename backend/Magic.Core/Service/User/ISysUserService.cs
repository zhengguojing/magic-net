﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysUserService : ITransient
{
    Task Add(AddUserInput input);
    Task Delete(PrimaryKeyParam input);
    Task Update(EditUserInput input);
    Task ChangeUserStatus(ChangeUserStatusInput input);
    Task<IActionResult> Export();
    Task Import(IFormFile file);
    Task<UserOutput> Get(PrimaryKeyParam input);
    Task<PageList<UserOutput>> PageList(QueryUserPageInput input);


    Task<List<long>> GetUserDataScopeIdList(long userId = 0);
    Task<dynamic> GetUserOwnData(PrimaryKeyParam input);
    Task<dynamic> GetUserOwnRole(PrimaryKeyParam input);
    Task<dynamic> GetUserSelector(QueryUserPageInput input);
    Task GrantUserData(GrantUserInput input);
    Task GrantUserRole(GrantUserInput input);
    Task ResetUserPwd(PrimaryKeyParam input);
    Task UpdateAvatar(UploadAvatarInput input);
    Task UpdateUserInfo(UpdateUserInfo input);
    Task UpdateUserPwd(ChangePasswordUserInput input);
    Task<List<long>> GetDataScopeIdUserList(long userId = 0);
    void CheckDataScopeByUserId(long userId);
    void CheckDataScope(long orgId);
}
