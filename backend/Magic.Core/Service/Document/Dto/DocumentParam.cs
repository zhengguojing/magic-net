﻿using Microsoft.AspNetCore.Http;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Magic.Core.Service;

#region 输入参数
/// <summary>
/// 文档输入参数
/// </summary>
public class QueryDocumentPageInput : PageParamBase
{
    /// <summary>
    /// 父Id
    /// </summary>
    public virtual long? Pid { get; set; } = 0;

    /// <summary>
    /// 名称
    /// </summary>
    public virtual string Name { get; set; }

    /// <summary>
    /// 标签
    /// </summary>
    public virtual int? Label { get; set; }

    /// <summary>
    /// 文件类型
    /// </summary>
    public string FileType { get; set; }

    public bool IsDelete { get; set; }

    /// <summary>
    /// 文档类型:文件、文件夹
    /// </summary>
    public DocumentType DocumentType { get; set; }

    public DateTime? CreateTimeStart { get; set; }
    public DateTime? UpdateTimeStart { get; set; }

    public DateTime? CreateTimeEnd { get; set; }
    public DateTime? UpdateTimeEnd { get; set; }


}

public class AddDocumentInput : IValidatableObject
{
    /// <summary>
    /// 父Id
    /// </summary>
    public long? PId { get; set; } = 0;
    /// <summary>
    /// 名称
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// 标签
    /// </summary>
    public int? Label { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public string Remark { get; set; }

    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
        if (Name.Split(".").Length > 1)
        {
            yield return new ValidationResult(
                "名称不能带'.'"
                , new[] { nameof(Name) }
            );
        }
    }
}

public class EditDocumentInput : AddDocumentInput
{
    /// <summary>
    /// Id
    /// </summary>
    [Required(ErrorMessage = "Id不能为空")]
    public long? Id { get; set; }
}

/// <summary>
/// 上传文件输入参数
/// </summary>
public class DocumentUploadInput : QueryDocumentPageInput
{
    /// <summary>
    /// 父Id
    /// </summary>
    public long? PId { get; set; } = 0;

    public List<IFormFile> Files { get; set; }
}


/// <summary>
/// 批量删除入参
/// </summary>
public class BatchDeleteDocumentInput
{
    /// <summary>
    /// Id列表
    /// </summary>
    [Required(ErrorMessage = "Id列表不能为空")]
    public List<long>? Ids { get; set; }

}

/// <summary>
/// 移动入参
/// </summary>
public class MoveDocumentInput
{
    /// <summary>
    /// Id列表
    /// </summary>
    [Required(ErrorMessage = "Id列表不能为空")]
    public List<long>? Ids { get; set; }

    /// <summary>
    /// 父Id
    /// </summary>
    public long? PId { get; set; }

}
#endregion

#region 输出参数
/// <summary>
/// 分页文档输出参数
/// </summary>
public class QueryDocumentPageOutput
{
    /// <summary>
    /// Id
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// 父Id
    /// </summary>
    public long PId { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 文档类型:文件、文件夹
    /// </summary>
    public DocumentType DocumentType { get; set; }


    /// <summary>
    /// 文件类型
    /// </summary>
    public string FileType { get; set; }

    /// <summary>
    /// 文件后缀
    /// </summary>
    public string FileSuffix { get; set; }

    /// <summary>
    /// 文件大小kb
    /// </summary>
    public int? FileSizeKb { get; set; }

    /// <summary>
    /// 文件大小kb
    /// </summary>
    public string FileSize { get; set; }

    /// <summary>
    /// 标签
    /// </summary>
    public int? Label { get; set; }

    /// <summary>
    /// 标签数组
    /// </summary>
    public string[] Tags { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public string Remark { get; set; }

    /// <summary>
    /// 创建时间 
    ///</summary>

    public DateTime? CreatedTime { get; set; }
    /// <summary>
    /// 更新时间 
    ///</summary>

    public DateTime? UpdatedTime { get; set; }
    /// <summary>
    /// 创建用户ID 
    ///</summary>
    public long? CreatedUserId { get; set; }
    /// <summary>
    /// 创建人用户名 
    ///</summary>
    public string CreatedUserName { get; set; }
    /// <summary>
    /// 修改人ID 
    ///</summary>
    public long? UpdatedUserId { get; set; }

    /// <summary>
    /// 修改人用户名 
    ///</summary>
    public string UpdatedUserName { get; set; }


}

/// <summary>
/// 树查询输出
/// </summary>
public class DocumentTreeOutPut
{
    /// <summary>
    /// Id
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// 父Id
    /// </summary>
    public long PId { get; set; }



    /// <summary>
    /// 名称
    /// </summary>
    public string Name { get; set; }

    public List<DocumentTreeOutPut> Children { get; set; }
}
#endregion
