﻿using System.ComponentModel.DataAnnotations;

namespace Magic.Core.Service;

#region 输入参数
public class QueryRolePageInput : PageParamBase
{

    /// <summary>
    /// 角色名
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// 唯一编码
    /// </summary>
    public string Code { get; set; }
}

public class AddRoleInput
{
    /// <summary>
    /// 名称
    /// </summary>
    [Required(ErrorMessage = "角色名称不能为空")]
    public string Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [Required(ErrorMessage = "角色编码不能为空")]
    public string Code { get; set; }

    public string Remark { get; set; }

    public int Sort { get; set; }
}

public class EditRoleInput : AddRoleInput
{
    public long Id { get; set; }
}


/// <summary>
/// 授权参数，菜单、数据
/// </summary>
public class GrantRoleInput : PermissionBaseInput
{
    /// <summary>
    /// 角色Id
    /// </summary>
    [Required(ErrorMessage = "角色Id不能为空")]
    public long Id { get; set; }

    /// <summary>
    /// 2：菜单按钮授权 3：api资源授权；来自枚举<see cref="MenuType"/>。
    /// </summary>
    public MenuType Type { get; set; }
}

public class OwnMenuInput {
    /// <summary>
    /// 角色Id
    /// </summary>
    [Required(ErrorMessage = "角色Id不能为空")]
    public long Id { get; set; }

    /// <summary>
    /// 2：菜单按钮授权 3：api资源授权；来自枚举<see cref="MenuType"/>。
    /// </summary>
    public MenuType Type { get; set; }
}
#endregion

#region 输出参数
/// <summary>
/// 登录用户角色参数
/// </summary>
public class RoleOutput
{
    /// <summary>
    /// Id
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    public string Code { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    public string Name { get; set; }

    public long SysUserId { get; set; }
}

#endregion
