﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ICodeGenConfigService: ITransient
{
    Task Add(CodeGenConfig input);
    void AddList(List<TableColumnOutput> tableColumnOuputList, SysCodeGen codeGenerate);
    Task Delete(long codeGenId);
    Task<SysCodeGenConfig> Detail(Core.PrimaryKeyParam input);
    Task<List<CodeGenConfig>> List(QueryCodeGenConfigInput input);
    Task Update(List<CodeGenConfig> inputList);
}
