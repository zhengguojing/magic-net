﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysDictTypeService: ITransient
{
    Task Add(AddDictTypeInput input);
    Task ChangeDictTypeStatus(UpdateDictTypeStatusInput input);
    Task Delete(Core.PrimaryKeyParam input);
    Task<List<DictTreeOutput>> GetDictTree();
    Task<SysDictType> Get(Core.PrimaryKeyParam input);
    Task<List<SysDictData>> GetDictTypeDropDown(QueryDropDownDictTypeInput input);
    Task<List<SysDictType>> List();
    Task<PageList<SysDictType>> PageList(QueryDictPageInput input);
    Task Update(EditDictTypeInput input);
}
