﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysExLogService : ITransient
{
    Task Clear();
    Task<PageList<SysLogEx>> PageList(QueryExLogPageInput input);
}
