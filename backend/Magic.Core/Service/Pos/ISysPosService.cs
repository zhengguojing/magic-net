﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysPosService : ITransient
{
    Task Add(AddPosInput input);
    Task Delete(Core.PrimaryKeyParam input);
    Task<SysPos> Get(Core.PrimaryKeyParam input);
    Task<List<SysPos>> List(QueryPosPageInput input);
    Task<PageList<SysPos>> PageList(QueryPosPageInput input);
    Task Update(EditPosInput input);
}
