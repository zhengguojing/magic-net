﻿using Furion.DependencyInjection;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface IAuthService : ITransient
{
    Task<CaptchaVerifyResult_back> GetCaptcha();
    Task<bool> GetCaptchaOpen();
    Task<LoginOutput> GetLoginUserAsync();
    Task<string> LoginAsync(LoginInput input);
    Task LogoutAsync();
    Task<CaptchaVerifyResult_back> VerificationCode(ClickWordCaptchaInput input);

    Task<string> SimulationLoginAsync(LoginInput input);
    Task<CaptchaVerifyResult> GetCaptchaSkia();
    Task<CaptchaVerifyResult> VerificationCodeSkia(ClickWordCaptchaInput input);
}
