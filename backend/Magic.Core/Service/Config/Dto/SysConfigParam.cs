﻿using System.ComponentModel.DataAnnotations;

namespace Magic.Core.Service;

/// <summary>
/// 参数配置
/// </summary>
public class QuerySysConfigPageInput : PageParamBase
{
    /// <summary>
    /// 名称
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    public string Code { get; set; }


    /// <summary>
    /// 常量所属分类的编码，来自于“常量的分类”字典
    /// </summary>
    public string GroupCode { get; set; }
}

public class AddSysConfigInput
{
    /// <summary>
    /// 名称
    /// </summary>
    [Required(ErrorMessage = "参数名称不能为空")]
    public string Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [Required(ErrorMessage = "参数编码不能为空")]
    public string Code { get; set; }

    /// <summary>
    /// 常量所属分类的编码，来自于“常量的分类”字典
    /// </summary>
    [Required(ErrorMessage = "GroupCode不能为空")]
    public string GroupCode { get; set; }

    /// <summary>
    /// 属性值
    /// </summary>
    [Required(ErrorMessage = "Value不能为空")]
    public string Value { get; set; }

    /// <summary>
    /// 是否是系统参数（Y-是，N-否）
    /// </summary>
    [Required(ErrorMessage = "SysFlag不能为空")]
    public string SysFlag { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public string Remark { get; set; }
}


public class EditSysConfigInput : AddSysConfigInput
{
    /// <summary>
    /// 配置Id
    /// </summary>
    [Required(ErrorMessage = "Id不能为空")]
    public long Id { get; set; }
}
