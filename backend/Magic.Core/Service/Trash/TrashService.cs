﻿
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.EventBus;
using Furion.FriendlyException;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SqlSugar;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Magic.Core.Service;

/// <summary>
/// 回收站服务
/// </summary>
public class TrashService : ITrashService
{
    private readonly SqlSugarRepository<Documentation> _rep;
    private readonly IDocumentService _documentService;
    public TrashService(SqlSugarRepository<Documentation> rep, IDocumentService documentService)
    {
        _rep = rep;
        _documentService = documentService;
    }
    #region API
    /// <summary>
    /// 分页查询回收站
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public async Task<PageList<QueryDocumentPageOutput>> Page(QueryDocumentPageInput input)
    {
        input.IsDelete = true;
        input.Pid = null;
        var result = await _documentService.Page(input);
        return result;
    }


    /// <summary>
    /// 恢复一个
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public async Task Recover(PrimaryKeyParam input)
    {
        var entity = await _rep.AsQueryable().Filter(null, true).Where(it => it.Id == input.Id).FirstAsync();

        if (entity == null)
            throw Oops.Bah(ErrorCode.D1002);

        var exist = await _rep.AnyAsync(it => it.PId == entity.PId && it.Name == entity.Name);
        if (exist)
        {
            if (entity.DocumentType == DocumentType.Folder)
            {
                entity.Name += $"({DateTime.Now.ToString("yyyyMMddHHmmss")})";//如果是文件夹直接后面加
            }
            else
            {
                var sp = entity.Name.Split(".");
                entity.Name = $"({sp[0]}{DateTime.Now.ToString("yyyyMMddHHmmss")}).{sp[1]}";
            }
        }
        entity.IsDeleted = false;
        await _rep.UpdateAsync(entity);

    }

    /// <summary>
    /// 恢复多个
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public async Task Recovers(BatchDeleteDocumentInput input)
    {
        var documents = await _rep.AsQueryable().Filter(null, true).Where(it => input.Ids.Contains(it.Id)).ToListAsync();
        var docGroup = documents.GroupBy(it => it.PId).ToList();
        docGroup.ForEach(it =>
        {
            var docs = it.ToList();
            var i = 0;
            docs.ForEach(doc =>
            {
                var document = documents.Where(u => u.Id == doc.Id).First();
                var existDb = _rep.Any(it => it.PId == doc.PId && it.Name == doc.Name);//先判断数据库有没有重名
                var exist = docs.Any(it => it.Id != doc.Id && it.Name == doc.Name);//判断列表有没有重名
                if (existDb || exist)
                {
                    document.Name = Ren(doc.Name, i);
                }
                document.IsDeleted = false;
                i++;
            });
        });
        await _rep.UpdateAsync(documents);
    }


    /// <summary>
    /// 永久删除
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public async Task Delete(PrimaryKeyParam input)
    {
        var file = await _rep.AsQueryable().Filter(null, true).Where(u => u.Id == input.Id).FirstAsync();
        if (file == null)
            throw Oops.Bah(ErrorCode.D1002);
        file.Visible = false;
        await _rep.UpdateAsync(file);//删除当前
    }

    /// <summary>
    ///批量删除
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public async Task Deletes(BatchDeleteDocumentInput input)
    {
        await _rep.UpdateAsync(it => input.Ids.Contains(it.Id), it => new Documentation
        {
            Visible = false
        });
    }

    /// <summary>
    /// 清空
    /// </summary>
    /// <returns></returns>
    public async Task Empty()
    {
        await _rep.UpdateAsync(it => it.IsDeleted == true, it => new Documentation
        {
            Visible = false
        });
    }
    #endregion
    #region 方法


    /// <summary>
    /// 重命名
    /// </summary>
    /// <param name="name"></param>
    /// <param name="i"></param>
    /// <returns></returns>
    private string Ren(string name, int i)
    {
        if (i == 0)
        {
            return name + $"({DateTime.Now.ToString("yyyyMMddHHmmss")})";
        }
        else
        {
            return name + $"({DateTime.Now.ToString("yyyyMMddHHmmss")})({i})";
        }

    }
    #endregion
}
