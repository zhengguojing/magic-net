﻿
using Furion.DependencyInjection;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
namespace Magic.Core.Service;

public interface ITrashService : ITransient
{
    Task Delete(PrimaryKeyParam input);
    Task<PageList<QueryDocumentPageOutput>> Page(QueryDocumentPageInput input);

    Task Recover(PrimaryKeyParam input);
    Task Recovers(BatchDeleteDocumentInput input);
    Task Deletes(BatchDeleteDocumentInput input);
    Task Empty();
}
