﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysMenuService : ITransient
{
    Task Add(AddSysMenuInput input);
    Task<List<AntDesignTreeNode>> ChangeAppMenu(ChangeAppMenuInput input);
    Task Delete(Core.PrimaryKeyParam input);
    Task<List<AntDesignTreeNode>> GetLoginMenusAntDesign(long userId, string appCode);
    Task<List<string>> GetLoginPermissionList(long userId);
    Task<SysMenu> Get(Core.PrimaryKeyParam input);
    Task<dynamic> List(QueryMenuListInput input);
    Task<List<MenuTreeOutput>> GetMenuTree(QueryMenuTreeInput input);
    Task<List<string>> GetUserMenuAppCodeList(long userId);
    Task<bool> HasMenu(string appCode);

    /// <summary>
    /// 获取系统菜单树，用于给角色授权时选择
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    Task<dynamic> TreeForGrant(MenuType type);

    Task UpdateMenu(EditSysMenuInput input);
    Task<List<string>> GetAllPermission();
}