﻿using SqlSugar;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Magic.Core.Entity;

/// <summary>
/// 代码生成字段配置表
/// </summary>
[SugarTable("sys_code_gen_config")]
[Description("代码生成字段配置表")]
public class SysCodeGenConfig : DEntityBase
{
    /// <summary>
    /// 代码生成主表ID
    /// </summary>
    [SugarColumn(ColumnDescription = "代码生成主表ID")]
    public long CodeGenId { get; set; }

    /// <summary>
    /// 数据库字段名
    /// </summary>
    [Required, MaxLength(255)]
    [SugarColumn(ColumnDescription = "数据库字段名", Length = 255)]
    public string ColumnName { get; set; }

    /// <summary>
    /// 字段描述
    /// </summary>
    [MaxLength(255)]
    [SugarColumn(ColumnDescription = "字段描述",IsNullable =true, Length = 255)]
    public string ColumnComment { get; set; }

    /// <summary>
    /// .NET数据类型
    /// </summary>
    [MaxLength(32)]
    [SugarColumn(ColumnDescription = ".NET数据类型", IsNullable = true, Length = 32)]
    public string NetType { get; set; }

    /// <summary>
    /// 作用类型（字典）
    /// </summary>
    [MaxLength(64)]
    [SugarColumn(ColumnDescription = "作用类型（字典）", IsNullable = true, Length = 64)]
    public string EffectType { get; set; }

    /// <summary>
    /// 外键实体名称
    /// </summary>
    [MaxLength(64)]
    [SugarColumn(ColumnDescription = "外键实体名称", IsNullable = true, Length = 64)]
    public string FkEntityName { get; set; }

    /// <summary>
    /// 外键显示字段
    /// </summary>
    [MaxLength(64)]
    [SugarColumn(ColumnDescription = "外键显示字段", IsNullable = true, Length = 64)]
    public string FkColumnName { get; set; }

    /// <summary>
    /// 外键显示字段.NET类型
    /// </summary>
    [MaxLength(64)]
    [SugarColumn(ColumnDescription = "外键显示字段.NET类型", IsNullable = true, Length = 64)]
    public string FkColumnNetType { get; set; }

    /// <summary>
    /// 字典code
    /// </summary>
    [MaxLength(64)]
    [SugarColumn(ColumnDescription = "字典code", IsNullable = true, Length = 64)]
    public string DictTypeCode { get; set; }

    /// <summary>
    /// 列表是否缩进（字典）
    /// </summary>
    [MaxLength(2)]
    [SugarColumn(ColumnDescription = "列表是否缩进（字典）", IsNullable = true, Length = 2)]
    public string WhetherRetract { get; set; }

    /// <summary>
    /// 是否必填（字典）
    /// </summary>
    [MaxLength(2)]
    [SugarColumn(ColumnDescription = "是否必填（字典）", IsNullable = true, Length = 2)]
    public string WhetherRequired { get; set; }

    /// <summary>
    /// 是否是查询条件
    /// </summary>
    [MaxLength(2)]
    [SugarColumn(ColumnDescription = "是否是查询条件", IsNullable = true, Length = 2)]
    public string QueryWhether { get; set; }

    /// <summary>
    /// 查询方式
    /// </summary>
    [MaxLength(16)]
    [SugarColumn(ColumnDescription = "查询方式", IsNullable = true, Length = 16)]
    public string QueryType { get; set; }

    /// <summary>
    /// 列表显示
    /// </summary>
    [MaxLength(2)]
    [SugarColumn(ColumnDescription = "列表显示", IsNullable = true, Length = 2)]
    public string WhetherTable { get; set; }

    /// <summary>
    /// 增改
    /// </summary>
    [MaxLength(2)]
    [SugarColumn(ColumnDescription = "增改", IsNullable = true, Length = 2)]
    public string WhetherAddUpdate { get; set; }

    /// <summary>
    /// 主键
    /// </summary>
    [MaxLength(8)]
    [SugarColumn(ColumnDescription = "主键", IsNullable = true,Length =8)]
    public string ColumnKey { get; set; }

    /// <summary>
    /// 数据库中类型（物理类型）
    /// </summary>
    [MaxLength(64)]
    [SugarColumn(ColumnDescription = "数据库中类型（物理类型）", IsNullable = true, Length = 64)]
    public string DataType { get; set; }

    /// <summary>
    /// 是否通用字段
    /// </summary>
    [MaxLength(2)]
    [SugarColumn(ColumnDescription = "是否通用字段", IsNullable = true, Length = 2)]
    public string WhetherCommon { get; set; }
    /// <summary>
    /// 是否是枚举
    /// </summary>
    public bool IsEnum { get; set; }


}
