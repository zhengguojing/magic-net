﻿using SqlSugar;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Magic.Core.Entity;

/// <summary>
/// 菜单表
/// </summary>
[SugarTable("sys_menu")]
[Description("菜单表")]
public class SysMenu : DEntityBase
{
    /// <summary>
    /// 父Id
    /// </summary>
    [SugarColumn(ColumnDescription = "父Id")]
    public long Pid { get; set; }

    /// <summary>
    /// 父Ids
    /// </summary>
    [SugarColumn(ColumnDescription = "父Ids", Length = 255)]
    public string Pids { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    [Required, MaxLength(32)]
    [SugarColumn(ColumnDescription = "名称", Length = 32)]
    public string Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [Required, MaxLength(64)]
    [SugarColumn(ColumnDescription = "编码", Length = 64)]
    public string Code { get; set; }

    /// <summary>
    /// 菜单类型（字典 0目录 1菜单 2按钮 3api资源），来自枚举<see cref="MenuType"/>。
    /// </summary>
    [SugarColumn(ColumnDescription = "菜单类型（字典 0目录 1菜单 2按钮 3api资源）")]
    public MenuType Type { get; set; }

    /// <summary>
    /// 图标
    /// </summary>
    [MaxLength(32)]
    [SugarColumn(ColumnDescription = "图标", IsNullable = true, Length = 32)]
    public string Icon { get; set; }

    /// <summary>
    /// 路由地址
    /// </summary>
    [MaxLength(255)]
    [SugarColumn(ColumnDescription = "路由地址", IsNullable = true, Length = 255)]
    public string Router { get; set; }

    /// <summary>
    /// 组件地址
    /// </summary>
    [MaxLength(255)]
    [SugarColumn(ColumnDescription = "组件地址", IsNullable = true, Length = 255)]
    public string Component { get; set; }

    /// <summary>
    /// 权限标识
    /// </summary>
    [MaxLength(255)]
    [SugarColumn(ColumnDescription = "权限标识", IsNullable = true, Length = 255)]
    public string Permission { get; set; }

    /// <summary>
    /// 应用分类（应用编码）
    /// </summary>
    [MaxLength(64)]
    [SugarColumn(ColumnDescription = "应用分类（应用编码）", IsNullable = true, Length = 64)]
    public string Application { get; set; }

    /// <summary>
    /// 打开方式（字典 0无 1组件 2内链 3外链）
    /// </summary>
    [SugarColumn(ColumnDescription = "打开方式（字典 0无 1组件 2内链 3外链）")]
    public int OpenType { get; set; } = 0;

    /// <summary>
    /// 是否可见（Y-是，N-否）
    /// </summary>
    [MaxLength(2)]
    [SugarColumn(ColumnDescription = "是否可见（Y-是，N-否）", IsNullable = true, Length = 2)]
    public string Visible { get; set; } = "Y";

    /// <summary>
    /// 内链地址
    /// </summary>
    [MaxLength(255)]
    [SugarColumn(ColumnDescription = "内链地址", IsNullable = true, Length = 255)]
    public string Link { get; set; }

    /// <summary>
    /// 重定向地址
    /// </summary>
    [MaxLength(255)]
    [SugarColumn(ColumnDescription = "重定向地址", IsNullable = true, Length = 255)]
    public string Redirect { get; set; }

    /// <summary>
    /// 权重（字典 1系统权重 2业务权重）
    /// </summary>
    [SugarColumn(ColumnDescription = "权重（字典 1系统权重 2业务权重）")]
    public int Weight { get; set; } = 2;

    /// <summary>
    /// 排序
    /// </summary>
    [SugarColumn(ColumnDescription = "排序")]
    public int Sort { get; set; } = 100;

    /// <summary>
    /// 备注
    /// </summary>
    [MaxLength(255)]
    [SugarColumn(ColumnDescription = "备注", IsNullable = true, Length = 255)]
    public string Remark { get; set; }

    /// <summary>
    /// 状态（字典 0正常 1停用 2删除）
    /// </summary>
    [SugarColumn(ColumnDescription = "状态（字典 0正常 1停用 2删除）")]
    public CommonStatus Status { get; set; } = CommonStatus.ENABLE;

    /// <summary>
    /// 多标签模式下是否开启缓存，默认开启
    /// </summary>
    [SugarColumn(ColumnDescription = "多标签模式下是否开启缓存，默认开启")]
    public bool KeepAlive { get; set; } = true;
}