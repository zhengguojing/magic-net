﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic.Core
{
    public class CaptchaVerifyResult
    {
        public string RepCode { get; set; } = "0000";
        public string RepMsg { get; set; }
        public RepData RepData { get; set; } = new RepData();
        public bool Error { get; set; }
        public bool Success { get; set; } = true;
    }

}
