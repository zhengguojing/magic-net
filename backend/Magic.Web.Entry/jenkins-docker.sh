#!/bin/bash

##版本号
_tag=1.0
##镜像名称
_imageName=magic-service

#停止容器
cid=$(sudo docker ps -a | grep ${_imageName} | awk '{print $1}')
if [ -n "$cid" ]
     then
     sudo docker stop ${cid}
fi

#删除容器
cid=$(sudo docker ps -a | grep ${_imageName} | grep "Exited"  | awk '{print $1}')
if [ -n "$cid" ]
     then
     sudo docker rm ${cid}
fi

#删除镜像
tags=`sudo docker images | awk -v iname=${_imageName} '($1 ~ iname) {print $1":"$2}'`
if [ -n "${tags}" ]
then
  for tag in ${tags}
  do
    sudo docker rmi ${tag}
  done
fi

#删除空镜像
cid=$(sudo docker images | grep "none"  | awk '{print $3}')
if [ -n "$cid" ]
     then
     sudo docker rmi ${cid}
fi

#编译代码
rm -rf publish

dotnet publish -r linux-x64 -f net7.0 -o ./publish --self-contained false

if [ $? -ne 0 ]
     then
     echo "compilation fails"
     exit 1
fi
#生成镜像
sudo docker build -t ${_imageName}:${_tag} .
if [ $? -ne 0 ]
     then
     echo "docker build fails"
     exit 1
fi

##启动镜像
echo '=====================run container==================='
sudo docker run -p 5000:80 \
-v /data/magic-service/logs:/app/Logs \
-v /data/Magic.db:/app/Magic.db \
-v /data/flow.db:/app/flow.db \
-e TZ=Asia/Shanghai \
--restart=always \
--name ${_imageName} -d ${_imageName}:${_tag}
