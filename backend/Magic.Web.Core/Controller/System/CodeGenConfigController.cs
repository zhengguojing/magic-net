﻿using Aliyun.OSS;
using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Entity;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Magic.Web.Core;

/// <summary>
/// 代码生成详细配置服务
/// </summary>
[ApiDescriptionSettings(Name = "CodeGenConfig", Order = 100,Tag = "代码生成详细配置服务")]
public class CodeGenConfigController : IDynamicApiController
{
    private readonly ICodeGenConfigService _service;
    public CodeGenConfigController(ICodeGenConfigService service)
    {
        _service = service;
    }

    /// <summary>
    /// 代码生成详细配置列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysCodeGenerateConfig/list")]
    public async Task<List<CodeGenConfig>> List([FromQuery] QueryCodeGenConfigInput input) {
        return await _service.List(input);
    }

    /// <summary>
    /// 更新
    /// </summary>
    /// <param name="inputList"></param>
    /// <returns></returns>
    [HttpPost("/sysCodeGenerateConfig/edit")]
    public async Task Update(List<CodeGenConfig> inputList) {
        await _service.Update(inputList);
    }

    /// <summary>
    /// 详情
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysCodeGenerateConfig/detail")]
    public async Task<SysCodeGenConfig> Detail([FromQuery] Magic.Core.PrimaryKeyParam input){
        return await _service.Detail(input);
    }
}
