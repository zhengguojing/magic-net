﻿using Furion;
using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 登录授权相关服务
/// </summary>
[ApiDescriptionSettings(Name = "Auth", Order = 100,Tag = "登录授权相关服务")]
public class AuthController : IDynamicApiController
{
    private readonly IAuthService _service;

    public AuthController(IAuthService service)
    {
        _service = service;
    }

    /// <summary>
    /// 用户登录
    /// </summary>
    /// <param name="input"></param>
    /// <remarks>默认用户名/密码：admin/admin</remarks>
    /// <returns></returns>
    [HttpPost("/login")]
    [AllowAnonymous]
    public async Task<string> LoginAsync([Required] LoginInput input) { 
        return await _service.LoginAsync(input);
    }

    /// <summary>
    /// 模拟租户登录
    /// </summary>
    /// <param name="input"></param>
    /// <remarks>默认用户名/密码：admin/admin</remarks>
    /// <returns></returns>
    [HttpPost("/simulationTenantLogin")]
    public async Task<string> SimulationLoginAsync([Required] LoginInput input) { 
        return await _service.SimulationLoginAsync(input);
    }

    /// <summary>
    /// 获取当前登录用户信息
    /// </summary>
    /// <returns></returns>
    [HttpGet("/getLoginUser")]
    public async Task<LoginOutput> GetLoginUserAsync() { 
        return await _service.GetLoginUserAsync();
    }

    /// <summary>
    /// 退出
    /// </summary>
    /// <returns></returns>
    [HttpGet("/logout")]
    public async Task LogoutAsync() { 
        await _service.LogoutAsync();
    }

    /// <summary>
    /// 获取验证码开关
    /// </summary>
    /// <returns></returns>
    [HttpGet("/getCaptchaOpen")]
    [AllowAnonymous]
    public async Task<bool> GetCaptchaOpen() { 
        return await _service.GetCaptchaOpen();
    }

    /// <summary>
    /// 获取验证码（默认点选模式）
    /// </summary>
    /// <returns></returns>
    [HttpPost("/captcha/get")]
    [AllowAnonymous]
    [NonUnify]
    public async Task<CaptchaVerifyResult> GetCaptcha() { 
        return await _service.GetCaptchaSkia();
    }

    /// <summary>
    /// 校验验证码
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/captcha/check")]
    [AllowAnonymous]
    [NonUnify]
    public async Task<CaptchaVerifyResult> VerificationCode(ClickWordCaptchaInput input) {
        return await _service.VerificationCodeSkia(input);
    }
}
