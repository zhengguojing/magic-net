﻿using Aliyun.OSS;
using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Magic.Web.Core;

/// <summary>
/// 系统菜单服务
/// </summary>
[ApiDescriptionSettings(Name = "Menu", Order = 100, Tag = "系统菜单服务")]
public class SysMenuController : IDynamicApiController
{
    private readonly ISysMenuService _service;

    public SysMenuController(ISysMenuService service)
    {
        _service = service;
    }

    /// <summary>
    /// 系统菜单列表（树表）
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysMenu/list")]
    public async Task<dynamic> List([FromQuery] QueryMenuListInput input)
    {
        return await _service.List(input);
    }

    /// <summary>
    /// 增加系统菜单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysMenu/add")]
    public async Task Add(AddSysMenuInput input)
    {
        await _service.Add(input);
    }

    /// <summary>
    /// 删除系统菜单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysMenu/delete")]
    public async Task Delete(PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    /// 更新系统菜单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysMenu/edit"),]
    public async Task Update(EditSysMenuInput input)
    {
        await _service.UpdateMenu(input);
    }

    /// <summary>
    /// 获取系统菜单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysMenu/detail")]
    public async Task<dynamic> Get(PrimaryKeyParam input)
    {
        return await _service.Get(input);
    }

    /// <summary>
    /// 获取系统菜单树，用于新增、编辑时选择上级节点
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysMenu/tree")]
    public async Task<dynamic> GetMenuTree([FromQuery] QueryMenuTreeInput input)
    {
        return await _service.GetMenuTree(input);
    }

    /// <summary>
    /// 获取系统菜单树，用于给角色授权时选择
    /// </summary>
    /// <returns></returns>
    [HttpGet("/sysMenu/treeForGrant")]
    public async Task<dynamic> TreeForGrant(MenuType type = MenuType.BTN)
    {
        return await _service.TreeForGrant(type);
    }

    /// <summary>
    /// 根据系统应用切换菜单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysMenu/change")]
    [AllowAnonymous]
    public async Task<List<AntDesignTreeNode>> ChangeAppMenu(ChangeAppMenuInput input)
    {
        return await _service.ChangeAppMenu(input);
    }
}