﻿using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Entity;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Web.Core;


/// <summary>
/// 角色服务
/// </summary>
[ApiDescriptionSettings(Name = "Role", Order = 100, Tag = "角色服务")]
public class SysRoleController : IDynamicApiController
{
    private readonly ISysRoleService _service;
    public SysRoleController(ISysRoleService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页获取角色列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysRole/page")]
    public async Task<PageList<SysRole>> QueryRolePageList([FromQuery] QueryRolePageInput input)
    {
        return await _service.PageList(input);
    }

    /// <summary>
    /// 角色下拉（用于授权角色时选择）
    /// </summary>
    /// <returns></returns>
    [HttpGet("/sysRole/dropDown")]
    public async Task<dynamic> GetRoleDropDown()
    {
        return await _service.GetRoleDropDown();
    }

    /// <summary>
    /// 增加角色
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysRole/add")]
    public async Task Add(AddRoleInput input)
    {
        await _service.Add(input);
    }

    /// <summary>
    /// 删除角色
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysRole/delete")]
    public async Task Delete(PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    /// 更新角色
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysRole/edit")]
    public async Task Update(EditRoleInput input)
    {
        await _service.Update(input);
    }

    /// <summary>
    /// 获取角色
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysRole/detail")]
    public async Task<SysRole> Get([FromQuery] PrimaryKeyParam input)
    {
        return await _service.Get(input);
    }

    /// <summary>
    /// 授权角色菜单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysRole/grantMenu")]
    public async Task GrantMenu(GrantRoleInput input)
    {
        await _service.GrantMenu(input);
    }

    /// <summary>
    /// 授权角色数据范围
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysRole/grantData")]
    public async Task GrantData(GrantRoleInput input)
    {
        await _service.GrantData(input);
    }

    /// <summary>
    /// 获取角色拥有菜单Id集合
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysRole/ownMenu")]
    public async Task<List<OwnMenuOutput>> OwnMenu([FromQuery] OwnMenuInput input)
    {
        return await _service.OwnMenu(input);
    }

    /// <summary>
    /// 获取角色拥有数据Id集合
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysRole/ownData")]
    public async Task<List<long>> OwnData([FromQuery] PrimaryKeyParam input)
    {
        return await _service.OwnData(input);
    }
}
