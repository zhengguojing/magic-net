﻿using Aliyun.OSS;
using Furion.DynamicApiController;
using Magic.Core.Entity;
using Magic.Core;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Magic.Web.Core;

/// <summary>
/// 异常日志服务
/// </summary>
[ApiDescriptionSettings(Name = "ExLog", Order = 100, Tag = "异常日志服务")]
public class SysExLogController : IDynamicApiController
{
    private readonly ISysExLogService _service;
    public SysExLogController(ISysExLogService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页查询异常日志
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysExLog/page")]
    public async Task<PageList<SysLogEx>> PageList([FromQuery] QueryExLogPageInput input)
    {
        return await _service.PageList(input);
    }

    /// <summary>
    /// 清空异常日志
    /// </summary>
    /// <returns></returns>
    [HttpPost("/sysExLog/delete")]
    public async Task Clear()
    {
        await _service.Clear();
    }
}
