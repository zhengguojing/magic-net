﻿using Furion.DynamicApiController;
using Magic.Core.Entity;
using Magic.Core;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 访问日志服务
/// </summary>
[ApiDescriptionSettings(Name = "VisLog", Order = 100, Tag = "访问日志服务")]
public class SysVisLogController : IDynamicApiController
{
    private readonly ISysVisLogService _service;
    public SysVisLogController(ISysVisLogService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页查询访问日志
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysVisLog/page")]
    public async Task<PageList<SysLogVis>> PageList([FromQuery] QueryVisLogPageInput input)
    {
        return await _service.PageList(input);
    }

    /// <summary>
    /// 清空访问日志
    /// </summary>
    /// <returns></returns>
    [HttpPost("/sysVisLog/delete")]
    public async Task Clear()
    {
        await _service.Clear();
    }
}
