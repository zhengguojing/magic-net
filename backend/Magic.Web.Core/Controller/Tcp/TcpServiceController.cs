﻿using Furion.DynamicApiController;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TouchSocket.Sockets;

namespace Magic.Web.Core
{
    /// <summary>
    /// 系统菜单服务
    /// </summary>
    [ApiDescriptionSettings("TcpService", Name = "Tcp", Order = 100, Tag = "Tcp服务器")]
    [AllowAnonymous]
    public class TcpServiceController : IDynamicApiController
    {
        private readonly ITcpService m_tcpService;

        public TcpServiceController(ITcpService tcpService)
        {
            this.m_tcpService = tcpService;
        }

        /// <summary>
        /// 获取所有连接到<see cref="ITcpService"/>的客户端Id。
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<string> GetClientIds()
        {
            return this.m_tcpService.GetIds();
        }

        /// <summary>
        /// 发送消息到指定Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="msg"></param>
        [HttpPost]
        public async Task SendMessage(string id, [FromBody] string msg)
        {
            if (!this.m_tcpService.TryGetClient(id, out var socketClient))
            {
                throw new Exception("没有找到对于Id的客户端");
            }

            await socketClient.SendAsync(msg);
        }
    }
}
