﻿using System;
using SqlSugar;
using System.ComponentModel;
using Magic.Core.Entity;
using Magic.Core;

namespace Magic.Application.Entity
{
    /// <summary>
    /// 
    /// </summary>
    [SugarTable("code_gen_test")]
    [Description("代码生成器测试表")]
    public class CodeGenTest : AutoIncrementEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Age { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Gender Gender { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime Birthday { get; set; }
    }
}