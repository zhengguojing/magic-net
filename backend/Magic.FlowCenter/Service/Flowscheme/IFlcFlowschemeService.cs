﻿using Furion.DependencyInjection;
using Magic.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Magic.FlowCenter.Service;

	public interface IFlcFlowschemeService: ITransient
{
    Task Add(AddFlcFlowschemeInput input);
    Task Delete(PrimaryKeyParam input);
    Task<FlcFlowschemeOutput> Get(PrimaryKeyParam input);
    Task<List<FlcFlowschemeOutput>> List(QueryFlcFlowschemePageInput input);
    Task<PageList<FlcFlowschemeOutput>> Page(QueryFlcFlowschemePageInput input);
    Task Update(EditFlcFlowschemeInput input);
}
